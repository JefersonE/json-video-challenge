const server = require('./express')

const port = process.env.SERVERPORT || 2000

server.listen(port)
console.log('Listening on ', port)