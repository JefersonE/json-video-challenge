const express = require('express')
const bodyParser = require('body-parser')

const jsonActions = require('./models/jsonActions')

const app = express()

app.set('view engine', 'ejs');
app.set('views', './views')

app.use( bodyParser.json() )
app.use( bodyParser.urlencoded( { extended: true } ) )

app.get('/', (req, res) => res.render('main', { json: '' }) )
app.post('/', jsonActions.processReq )

module.exports = app